# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150504001400) do

  create_table "answer_validations", force: true do |t|
    t.integer  "answer_id"
    t.integer  "user_id"
    t.boolean  "is_valid"
    t.string   "reference"
    t.string   "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "correct_answer"
    t.integer  "num_upvotes",    default: 0
    t.integer  "num_downvotes",  default: 0
  end

  create_table "answers", force: true do |t|
    t.string   "question"
    t.string   "answer"
    t.float    "difficulty"
    t.string   "reference"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "difficulty_votes", default: 1
  end

  create_table "answers_game_board_categories", force: true do |t|
    t.integer "answer_id"
    t.integer "game_board_category_id"
  end

  create_table "game_board_categories", force: true do |t|
    t.string   "name"
    t.string   "tags"
    t.integer  "category_index"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "game_boards", force: true do |t|
    t.string   "name"
    t.boolean  "double_jeopardy"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public_playable"
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.string   "object_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "object_id"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
  end

  create_table "votes", force: true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.boolean  "upvote"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
