class AddObjectIdToTags < ActiveRecord::Migration
  def change
    add_column :tags, :object_id, :integer
  end
end
