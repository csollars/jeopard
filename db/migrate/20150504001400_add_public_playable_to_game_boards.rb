class AddPublicPlayableToGameBoards < ActiveRecord::Migration
  def change
    add_column :game_boards, :public_playable, :boolean
  end
end
