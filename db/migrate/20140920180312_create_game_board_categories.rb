class CreateGameBoardCategories < ActiveRecord::Migration
  def change
    create_table :game_board_categories do |t|
      t.string :name
      t.string :tags
      t.integer :category_index

      t.timestamps
    end
  end
end
