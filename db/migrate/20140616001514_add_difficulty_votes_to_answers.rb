class AddDifficultyVotesToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :difficulty_votes, :integer, default: 1
    change_column :answers, :difficulty, :float
  end
end
