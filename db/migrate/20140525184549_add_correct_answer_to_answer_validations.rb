class AddCorrectAnswerToAnswerValidations < ActiveRecord::Migration
  def change
    add_column :answer_validations, :correct_answer, :string
  end
end
