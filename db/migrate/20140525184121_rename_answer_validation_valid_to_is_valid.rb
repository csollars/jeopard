class RenameAnswerValidationValidToIsValid < ActiveRecord::Migration
  def self.up
    rename_column :answer_validations, :valid, :is_valid
  end

  def self.down
    rename_column :answer_validations, :is_valid, :valid
  end
end
