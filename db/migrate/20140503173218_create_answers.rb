class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :question
      t.string :answer
      t.integer :difficulty
      t.string :reference

      t.timestamps
    end
  end
end
