class AddVoteCountsToAnswerValidations < ActiveRecord::Migration
  def change
    add_column :answer_validations, :num_upvotes, :integer, :default => 0
    add_column :answer_validations, :num_downvotes, :integer, :default => 0
  end
end
