class CreateGameBoards < ActiveRecord::Migration
  def change
    create_table :game_boards do |t|
      t.string :name
      t.boolean :double_jeopardy

      t.timestamps
    end
  end
end
