class CreateAnswerValidations < ActiveRecord::Migration
  def change
    create_table :answer_validations do |t|
      t.integer :answer_id
      t.integer :user_id
      t.boolean :valid
      t.string :reference
      t.string :reason

      t.timestamps
    end
  end
end
