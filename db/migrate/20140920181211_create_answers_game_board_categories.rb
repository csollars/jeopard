class CreateAnswersGameBoardCategories < ActiveRecord::Migration
  def change
    create_table :answers_game_board_categories do |t|
      t.integer :answer_id
      t.integer :game_board_category_id
    end
  end
end
