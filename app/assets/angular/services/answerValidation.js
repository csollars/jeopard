'use strict'
var app = angular.module('app');
app.factory('AnswerValidation', function($resource) {
	return $resource('/api/v1/answer_validations/:id',
        {id: '@id'},
        {
            update:{method: 'PUT'},
            register: {method: 'POST'}
        });
});