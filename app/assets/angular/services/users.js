'use strict'
var app = angular.module('app');
app.factory('User', function($resource) {
	return $resource('/api/v1/users/:id',
        {id: '@id'},
        {
            update:{method: 'PUT'},
            register: {method: 'POST', params:{register: true}}
        });
});
app.factory('LoggedUser', function() {
    return {
        logged_in: false,
        user: {}
    }
});

app.directive('passwordValidate', function() {
    return {
        require: 'ngModel',
        link: function($scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {

                $scope.pwdValidLength = (viewValue && viewValue.length >= 6 ? 'valid' : undefined);

                if($scope.pwdValidLength) {
                    ctrl.$setValidity('pwd', true);
                    return viewValue;
                } else {
                    ctrl.$setValidity('pwd', false);                    
                    return undefined;
                }

            });
        }
    };
});

app.directive('passwordRepeat', function() {
	return {
		require: 'ngModel',
		link: function ($scope, elm, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {
                if($('#password').val() == viewValue) {
                    ctrl.$setValidity('pwd_repeat', true);
                    return viewValue;
                } else {
                    ctrl.$setValidity('pwd_repeat', false);
                    return undefined;
                }
			});
		}
	}
});

app.directive('unique', function() {
    return {
        require: 'ngModel',
        link: function ($scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                ctrl.$setValidity('unique', true);
            });
        }
    }
});