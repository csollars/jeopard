'use strict'
var app = angular.module('app');

app.controller('MyQuestionsController', function($scope, $location, User, Answer, AnswerValidation, appStorage, $http) {
	var storage = appStorage('Jeopardy');

	var answers_recv = function(data) {
		$scope.num_answers = data.length;
		$scope.answers = {};
		angular.forEach(data, function(answer) {
			$scope.answers[answer.id] = answer;
		});
	}

	var confirmed_invalid_recv = function(data) {
		$scope.invalid_answers = {};
		$scope.num_invalid_answers = data.length;
		angular.forEach(data, function(answer) {
			$scope.invalid_answers[answer.id] = answer;
		});
	}

	$scope.user = storage.get('user');
	$scope.new_answer = {difficulty: 5};
	console.log($scope.user);
	if($scope.user == undefined || $scope.user == null) {
		$location.path('/login');
		return;
	}

	$scope.answers = {};
	$scope.num_answers = 0;

	$scope.invalid_answers = {};
	$scope.num_invalid_answers = 0;

	console.log(Answer.service);
	Answer.query({user_id: $scope.user.id, recent: true}).$promise.then(answers_recv);

	Answer.query({user_id: $scope.user.id, confirmed_invalid: true}).$promise.then(confirmed_invalid_recv);

	$scope.logout = function() {
		$http.get('/api/v1/logout');
		storage.set('user', null);
		$location.path('/login');
	}

	$scope.edit_answer = function(answer_id) {
		$scope.edit_answer_modal.answer_id = answer_id;
	}

	$scope.edit_answer_modal = {
		answer_id: 0,
		answer: {},
		save_success: false,
		set_current: function(answer_id) {
			$scope.edit_answer_modal.save_success = false;
			$scope.edit_answer_modal.answer_id = answer_id;
			if(answer_id) {
				$scope.edit_answer_modal.answer = $scope.answers[answer_id];
				if(!$scope.edit_answer_modal.answer.invalid_validations) {
					$scope.edit_answer_modal.answer.invalid_validations = [];
					AnswerValidation.query({answer_id: $scope.edit_answer_modal.answer.id, invalid: true}).$promise.then(function(validations) {
						angular.forEach(validations, function(v) {
							$scope.edit_answer_modal.answer.invalid_validations.push(v);
						});
						console.log($scope.edit_answer_modal.answer.invalid_validations);
					});
				}
			}
		},
		submit_answer: function() {
			var answer = new Answer($scope.edit_answer_modal.answer);
			answer.id = $scope.edit_answer_modal.answer_id;
			answer.update({fix_downvote: true}, function(response) {
				$scope.edit_answer_modal.save_success=true;
				Answer.query({user_id: $scope.user.id, confirmed_invalid: true}).$promise.then(confirmed_invalid_recv);
				Answer.query({user_id: $scope.user.id, recent: true}).$promise.then(answers_recv);
				if($scope.answer_form) $scope.answer_form.$setPristine();
			});
		}
	}
});