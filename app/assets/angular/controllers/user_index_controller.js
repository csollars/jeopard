'use strict'
var app = angular.module('app');

app.controller('UserIndexController', function($scope, $location, User, Answer, AnswerValidation, appStorage, $http, Vote) {
	var storage = appStorage('Jeopardy');
	$scope.user = storage.get('user');
	$scope.new_answer = {difficulty: 5};
	
	$scope.answer_difficulties = [1,2,3,4,5,6,7,8,9,10];
	console.log($scope.user);
	if($scope.user == undefined || $scope.user == null) {
		$location.path('/login');
		return;
	}

	$scope.validate_answer_modal = {
		answer_id: 0,
		answer: {},
		set_current: function(answer_id) {
			$scope.validate_answer_modal.answer_id = answer_id;
			if(answer_id) {
				if($scope.validatable_answers[answer_id].invalid_validations == undefined) {
					$scope.validatable_answers[answer_id].invalid_validations = [];
					$scope.validatable_answers[answer_id].difficulty = Math.round($scope.validatable_answers[answer_id].difficulty);
					AnswerValidation.query({answer_id: $scope.validatable_answers[answer_id].id, invalid: true, voter_id: $scope.user.id}).$promise.then(function(data) {
						angular.forEach(data, function(validation) {
							validation.user_upvoted = false;
							validation.user_downvoted = false;
							validation.upvote = function() {
								if(validation.user_downvoted || validation.user_upvoted) return;
								validation.vote = {
									user_id: $scope.user.id,
									votable_id: validation.id,
									votable_type: "AnswerValidation",
									upvote: true
								}
								var obj = new Vote(validation.vote);
								obj.$register();
								validation.user_upvoted = true;
								validation.num_upvotes++;
							}
							validation.downvote = function() {
								if(validation.user_downvoted || validation.user_upvoted) return;
								validation.vote = {
									user_id: $scope.user.id,
									votable_id: validation.id,
									votable_type: "AnswerValidation",
									upvote: false
								}
								var obj = new Vote(validation.vote);
								obj.$register();
								validation.user_downvoted = true;
								validation.num_downvotes++;
							}
							Vote.by_user_and_votable({user_id: $scope.user.id, votable_type: 'AnswerValidation', votable_id: validation.id}).$promise.then(function(data) {
								angular.forEach(data, function(vote) {
									if(vote.upvote) {
										validation.user_upvoted = true;
									} else {
										validation.user_downvoted = true;
									}
								});
								console.log(data);
								console.log(validation);
							})
							$scope.validatable_answers[answer_id].invalid_validations.push(validation);
						});
					});
				}
				$scope.validate_answer_modal.answer = $scope.validatable_answers[answer_id];
			}
		},
		confirm_validation: function() {
			delete $scope.validatable_answers[$scope.validate_answer_modal.answer_id];
			$scope.validate_answer_modal.set_current(0);
			$scope.num_validatable_answers--;
		},
		validate_next_answer: function() {
			delete $scope.validatable_answers[$scope.validate_answer_modal.answer_id];
			$scope.num_validatable_answers--;
			var answer_id = 0;
			for(var k in $scope.validatable_answers) {
				answer_id = k;
				break;
			}
			$scope.validate_answer_modal.set_current(answer_id);
		}
	}

	$scope.logout = function() {
		$http.get('/api/v1/logout');
		storage.set('user', null);
		$location.path('/login');
	}

	$scope.reset_answer_form = function() {
		$scope.answer = angular.copy($scope.new_answer);
		if($scope.answer_form) $scope.answer_form.$setPristine();
	}

	$scope.submit_answer = function () {
		var answer = new Answer($scope.answer);
		answer.tags = answer.tags_combined.split(",");
		for(var k in answer.tags) {
			answer.tags[k] = $.trim(answer.tags[k]);
		};
		answer.register(
			// Success
			function(response){
				console.log(response);
				$scope.reset_answer_form();
			},
			function(response) {
				console.log(response);
			});
	}

	$scope.validatable_answers = {};
	var validatable_ids = [];
	$scope.num_validatable_answers = 0;
	var validatable_answers_recv = function(data) {
		angular.forEach(data, function(answer) {
			if(answer.user_id != $scope.user.id) {
				$scope.num_validatable_answers++;
				validatable_ids.push(answer.id);
				answer.validation = {
					user_id: $scope.user.id,
					answer_id: answer.id
				};
				answer.set_validation = function(valid) {
					answer.validation.validated = true;
					answer.validation.is_valid = valid;
					if(valid) {
						answer.validation.confirmed = true;
						answer.submit_validation();
					}
				}
				answer.confirm_invalid = function() {
					answer.validation.confirmed = true;
					if(answer.validation.reason) answer.submit_validation();
				}
				answer.unset_validation = function() {
					answer.validation = {};
				}
				answer.submit_validation = function() {
					var obj = new AnswerValidation(answer.validation);
					var ans = new Answer(answer);
					ans.update({difficulty_vote: true});
					obj.$register(function(response) {console.log("Validation Submitted")})
				}
				$scope.validatable_answers[answer.id] = answer;
			}
		});
	}
	Answer.query({unvalidated_by: $scope.user.id, limit: 5}).$promise.then(validatable_answers_recv);

	$scope.refresh_answers = function() {
		var promise = Answer.query({unvalidated_by: $scope.user.id, limit: 5, "filter[]": validatable_ids}).$promise;
		validatable_ids = [];
		$scope.validatable_answers = {};
		promise.then(validatable_answers_recv);
	}

	$scope.reset_answer_form();
});