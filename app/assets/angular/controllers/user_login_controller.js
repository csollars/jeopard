'use strict'
var app = angular.module('app');

app.controller('UserLoginController', function($scope, $location, $http, User, appStorage) {
	var storage = appStorage('Jeopardy');
	$scope.user = {};
	$scope.login_user = {};
	$scope.register_attempted = false;
	$scope.registerUser = function() {
		$scope.register_attempted = true;
		$scope.repeat_password = null;
		for(var fieldname in $scope.user) {
			console.log(fieldname);
			for(var k in $scope.register[fieldname].$error) {
				$scope.register[fieldname].$setValidity(k, true);
			}
		}
		var obj = new User($scope.user);
		obj.$register(function(response) {
			console.log(response);
			$scope.user = response.user;
			storage.set('user', $scope.user);
			storage.set('token', response.token);
			$location.path('/');
		}, function(response) {
			if(response.data.errors != undefined) {
				for(var fieldname in response.data.errors) {
					for(var i in response.data.errors[fieldname]) {
						switch(response.data.errors[fieldname][i]) {
							case 'is invalid':
								$scope.register[fieldname].$setValidity('valid', false);
								break;
							case 'has already been taken':
								$scope.register[fieldname].$setValidity('unique', false);
								break;
						}
					}
				}
			}
		});
	};

	$scope.authenticate = function() {
		console.log($scope.login_user);
		$scope.login.username.$setValidity('valid', true);
		$scope.login.password.$setValidity('valid', true);
		$http.post('/api/v1/authenticate', $scope.login_user).
			success(function(response) {
				storage.set('user', response.user);
				storage.set('token', response.token);
				$location.path('/');
			}).
			error(function(response) {
				if(response.errors.username) {
					console.log("uname error");
					$scope.login.username.$setValidity('valid', false);
					$scope.login.password.$setValidity('valid', false);
				}
				if(response.errors.authentication) {
					if(response.errors.authentication == "not found") {
						console.log("not found");
						$scope.login.username.$setValidity('valid', false);
						$scope.login.password.$setValidity('valid', false);
					}
					if(response.errors.authentication == "bad credentials") {
						console.log("bad_pass");
						$scope.login.password.$setValidity('valid', false);
					}
				}
			});
	};
});