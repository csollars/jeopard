'use strict'
var app = angular.module('app');
var tags = [];
var select_tag;
app.controller('EditGameController', function($scope, $location, $routeParams, User, Answer, AnswerValidation, Tag, appStorage, $http) {
	var storage = appStorage('Jeopardy');
	$scope.game_id = $routeParams.game_id;
	$scope.round = "round_1";
	$scope.multiplier = 1;
	$scope.public_playable = "false"
	$scope.complete = false;
	console.log($scope.round);
	$scope.game_board = {
		name: "",
		public_playable: "false",
		round_1: {
			multiplier: 1,
			max_category: 6,
			num_questions: 5,
			categories: {
				1: {
					title: 'Category',
					tags: [],
					questions: []
				},
				2: {
					title: 'Category',
					tags: [],
					questions: []
				},
				3: {
					title: 'Category',
					tags: [],
					questions: []
				},
				4: {
					title: 'Category',
					tags: [],
					questions: []
				},
				5: {
					title: 'Category',
					tags: [],
					questions: []
				},
				6: {
					title: 'Category',
					tags: [],
					questions: []
				}
			}
		},
		round_2: {
			multiplier: 2,
			max_category: 6,
			num_questions: 5,
			categories: {
				1: {
					title: 'Category',
					tags: [],
					questions: []
				},
				2: {
					title: 'Category',
					tags: [],
					questions: []
				},
				3: {
					title: 'Category',
					tags: [],
					questions: []
				},
				4: {
					title: 'Category',
					tags: [],
					questions: []
				},
				5: {
					title: 'Category',
					tags: [],
					questions: []
				},
				6: {
					title: 'Category',
					tags: [],
					questions: []
				}
			}
		},
		round_3: {
			max_category: 1,
			num_questions: 1,
			categories: {
				1: {
					title: 'N/A',
					tags: [],
					questions: []
				}
			}
		}
	};

	select_tag = function(tag_name) {
		$scope.edit_board_modal.category.tags.push(tag_name);
		angular.element('#category_tags').val('');
		$scope.$apply();
	}

	$scope.tag_input = ""

	$scope.edit_board_modal = {
		category_id: 0,
		category: {},
		round: "round_1",
		set_current: function(category_id, round) {
			console.log("CROUND: " + round);
			$scope.edit_board_modal.category_id = category_id;
			$scope.edit_board_modal.category = angular.copy($scope.game_board[$scope.round].categories[category_id]);
			$scope.edit_board_modal.round = round;
			if(category_id == 0) $scope.edit_board_modal.category = {};
			angular.element('#category_tags').val('');
		},
		save: function() {
			var id = $scope.edit_board_modal.category_id;
			var rid = $scope.edit_board_modal.round;
			$scope.game_board[$scope.edit_board_modal.round].categories[id] = angular.copy($scope.edit_board_modal.category);
			$scope.edit_board_modal.set_current(0);
			var answer_ids = [];
			console.log("ROUND: " + rid);
			for(var i=1; i<=$scope.game_board[rid].max_category; i++) {
				var cat = $scope.game_board[rid].categories[i];
				console.log(rid);
				for(var k in cat.questions) {
					console.log(cat.questions[k]);
					if(cat.questions[k].id) answer_ids.push(cat.questions[k].id);
				}
			}
			Answer.query({valid: true, limit: $scope.game_board[rid].num_questions, "tags[]": $scope.game_board[rid].categories[id].tags, "exclude[]": answer_ids}).$promise.then(function(data) {
				$scope.game_board[rid].categories[id].questions = data;
				var categories_complete = 0;
				angular.forEach(["round_1", "round_2"], function(round) {
					angular.forEach([1,2,3,4,5,6], function(cat_id) {
						if($scope.game_board[round].categories[cat_id].questions.length == 5) categories_complete+= 1;
					});
				});
				if($scope.game_board.round_3.categories[1].questions.length == 1) categories_complete+= 1;
				if(categories_complete == 12 && $scope.game_board.name != "") $scope.complete = true;
			});
		},
		remove_tag: function(tag_name) {
			var new_tags = [];
			angular.forEach($scope.edit_board_modal.category.tags, function(tag) {
				if(tag != tag_name) new_tags.push(tag);
			});
			$scope.edit_board_modal.category.tags = new_tags;
			angular.element('#category_tags').val('');
		}
	},

	$scope.question_modal = {
		question: {id: 0},
		set_current: function(category_id, qid) {
			$scope.question_modal.question = $scope.game_board[$scope.round].categories[category_id].questions[qid];
		},
		unset: function() {
			$scope.question_modal.question = {id: 0};
		}
	}

	$scope.set_round = function(round) {
		if(round = "round_1") {
			$scope.round = "round_1";
			$scope.multiplier = 1;
		}
		if(round = "round_2") {
			$scope.round = "round_2";
			$scope.multiplier = 2;
		}
	}

	$scope.save = function() {
		console.log($scope.game_board);
	}

	Tag.query({}, function(data) {
		angular.forEach(data, function(tag) {
			tags.push(tag.name);
		});
	});
});