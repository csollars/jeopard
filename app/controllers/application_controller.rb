require 'jwt'

class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception
	after_filter :set_csrf_cookie_for_ng

	def set_csrf_cookie_for_ng
		cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
	end

	def validate_api_token
		error = false
		msg = ""

		if !request.headers['Authorization']
			error = true
			msg = "Not Authorized"
		end

		if !error
			bearer, token = request.headers['Authorization'].split(" ")
			data = JWT.decode(token, "jeopardy_dev")[0]
			@user = User.find_by_username_and_id(data["username"], data["id"])
			if !@user
				error = true
				msg = "User Not Found"
			end
		end

		if error
			respond_to do |format|
				format.json { render json: {:error => msg}, :status => 401 }
			end
		end
	end

	def index
		render :layout => 'application', :nothing => true
	end

	protected

	def verified_request?
		super || form_authenticity_token == request.headers['X-XSRF-TOKEN']
	end
end
