module Api
	module V1
		class VotesController < InheritedResources::Base
			respond_to :json
			before_filter :validate_api_token

			def index
				votes = Vote
				votes = votes.by_user(params[:user_id]) if params[:user_id]
				votes = votes.by_votable(params[:votable_id], params[:votable_type]) if params[:votable_id] and params[:votable_type]
				votes = votes.all

				respond_to do |format|
					format.json { render :json => votes.to_json }
				end
			end

			def resource_params
				params.require(:vote).permit(:user_id, :votable_id, :votable_type, :upvote)
			end

			def create
				@vote = Vote.new(resource_params)
				if @vote.valid?
					votable = @vote.votable
					votable.num_upvotes+=1 if @vote.up?
					votable.num_downvotes+=1 if @vote.down?
					votable.save!

					respond_to do |format|
						format.json { render :json => @vote }
					end
				else
					respond_to do |format|
						format.json { render :json => { :errors => @vote.errors }, :status => 400 }
					end
				end
			end
		end
	end
end