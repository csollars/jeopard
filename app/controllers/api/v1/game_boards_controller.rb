module Api
	module V1
		class GameBoardsController < InheritedResources::Base
			respond_to :json
			before_filter :validate_api_token

			def index
			end
		end
	end
end