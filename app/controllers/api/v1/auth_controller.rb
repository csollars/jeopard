require 'jwt'

module Api
	module V1
		class AuthController < ActionController::Base
			def authenticate
				puts "PARAMS: #{params.inspect}"
				@errors = {}
				@errors[:username] = "required" unless params[:auth][:username]
				@errors[:password] = "required" unless params[:auth][:password]
				if @errors.empty?
					@user = User.find_by_username(params[:auth][:username])
					@errors[:authentication] = "not found" if @user.nil?
				end

				if @errors.empty?
					@errors[:authentication] = "bad credentials" unless @user.authenticate(params[:auth][:password])
				end

				if @errors.empty?
					token = JWT.encode({:username => @user.username, :id => @user.id}, "jeopardy_dev")
					respond_to do |format|
						format.json { render :json => {:user=>@user, :token=>token}.to_json }
					end
				else
					respond_to do |format|
						format.json { render :json => {:errors=>@errors}.to_json, :status=>422 }
					end
				end
			end

			def logout
				reset_session
				render :text=>""
			end
		end
	end
end