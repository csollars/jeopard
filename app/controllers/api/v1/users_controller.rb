require 'jwt'

module Api
	module V1
		class UsersController < InheritedResources::Base
			respond_to :json
			before_filter :validate_api_token, except: [:create]

			def resource_params
				params.require(:user).permit(:username, :first_name, :last_name, :email, :password, :password_confirmation)
			end

			def create
				@user = User.new(resource_params)
				@user.password = params[:password]
				@user.password_confirmation = params[:password_confirmation]
				if @user.valid?
					@user.save!
					token = JWT.encode({:username => @user.username, :id => @user.id}, "jeopardy_dev")
					respond_to do |format|
						format.json { render :json => {:user=>@user, :token=>token}.to_json }
					end
				else
					respond_to do |format|
						format.json { render :json => {:errors=>@user.errors.as_json}.to_json, :status => 422 }
					end
				end
			end
		end
	end
end
