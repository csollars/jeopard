module Api
	module V1
		class TagsController < InheritedResources::Base
			respond_to :json
			before_filter :validate_api_token

			def index
				tags = Tag.select(:name).distinct

				respond_to do |format|
					format.json { render json: tags }
				end
			end
		end
	end
end