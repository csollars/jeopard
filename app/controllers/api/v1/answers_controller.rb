module Api
	module V1
		class AnswersController < InheritedResources::Base
			respond_to :json
			before_filter :validate_api_token

			def resource_params
				params.require(:answer).permit(:question, :answer, :difficulty, :reference)
			end

			def index
				@answers = Answer
				@answers = @answers.unvalidated_by_user(params[:unvalidated_by]) if params[:unvalidated_by]
				@answers = @answers.by_user(params[:user_id]) if params[:user_id]
				@answers = @answers.with_upvotes if params[:valid]
				@answers = @answers.with_downvotes if params[:invalid]
				@answers = @answers.with_confirmed_downvotes if params[:confirmed_invalid]
				@answers = @answers.order(:updated_at) if params[:recent]
				@answers = @answers.limit(params[:limit]) if params[:limit]
				@answers = @answers.filter(params[:filter]) if params[:filter]
				@answers = @answers.exclude(params[:exclude]) if params[:exclude]

				if params[:tags]
					params[:tags].each do |tag_name|
						@answers = @answers.with_tag(tag_name)
					end
				end
				@answers = @answers.order("RAND()")
				@answers = @answers.all

				respond_to do |format|
					format.json { render json: @answers.to_json(:methods=>[:upvotes, :downvotes, :tags_combined]) }
				end
			end

			def update
				answer = Answer.find(params[:id])
				difficulty = answer.difficulty
				answer.update_attributes(resource_params)
				if params[:difficulty_vote]
					votes = answer.difficulty_votes+1
					diff_total = (difficulty * answer.difficulty_votes) + resource_params[:difficulty]
					answer.difficulty = diff_total / votes
					answer.difficulty_votes = votes
				end

				if params[:fix_downvote]
					answer.answer_validations.each do |dv|
						dv.destroy unless dv.is_valid
					end
				end

				if answer.valid?
					answer.save!
					answer.update_tags(params[:answer][:tags]) if params[:answer][:tags]
					respond_to do |format|
						format.json { render json: answer.to_json }
					end
				else
					respond_to do |format|
						format.json { render :json => {:errors=>answer.errors.as_json}.to_json, :status => 422 }
					end
				end
			end

			def create
				answer = Answer.new(resource_params)
				answer.user_id = @user.id
				if answer.valid?
					answer.save!
					answer.update_tags(params[:answer][:tags]) if params[:answer][:tags]
					respond_to do |format|
						format.json { render :json => answer.to_json }
					end
				else
					respond_to do |format|
						format.json { render :json => {:errors=>answer.errors.as_json}.to_json, :status => 422 }
					end
				end
			end
		end
	end
end