module Api
	module V1
		class AnswerValidationsController < InheritedResources::Base
			respond_to :json
			before_filter :validate_api_token

			def resource_params
				params.require(:answer_validation).permit(:user_id, :answer_id, :is_valid, :correct_answer, :reference, :reason)
			end

			def index
				validations = AnswerValidation
				validations = validations.by_answer(params[:answer_id]) if params[:answer_id]
				validations = validations.valid if params[:valid]
				validations = validations.invalid if params[:invalid]

				validations = validations.all
				respond_to do |format|
					format.json { render :json => validations.to_json(methods: [:display_reason, :num_upvotes, :num_downvotes]) }
				end
			end

			def create
				@validation = AnswerValidation.new(resource_params)
				if @validation.valid?
					@validation.save!
					respond_to do |format|
						format.json { render :json => @validation.to_json }
					end
				else
					respond_to do |format|
						format.json { render :json => {:errors=>@validation.errors.as_json}.to_json, status: 422 }
					end
				end
			end
		end
	end
end