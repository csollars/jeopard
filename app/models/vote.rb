class Vote < ActiveRecord::Base
	belongs_to :votable, polymorphic: true

	scope :up, -> { where(:upvote => true) }
	scope :down, -> { where(:upvote => false) }
	scope :by_user, -> (user_id) { where(:user_id => user_id) }
	scope :by_votable, -> (votable_id, votable_type) { where(:votable_id => votable_id).where(:votable_type => votable_type) }

	def up?
		self.upvote
	end

	def down?
		!self.upvote
	end
end
