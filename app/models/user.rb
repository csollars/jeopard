class User < ActiveRecord::Base

	validates :username, presence: true
	validates :username, uniqueness: true, on: :create
	validates :first_name, presence: true
	validates :last_name, presence: true
	validates :email, presence: true
	validates :email, uniqueness: true, on: :create
	validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
	validates :password, presence: true
	validates :password_confirmation, presence: true

	has_secure_password
	has_many :answers
end
