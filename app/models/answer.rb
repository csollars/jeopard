class Answer < ActiveRecord::Base
	has_many :tags, as: :object
	has_many :answer_validations
	belongs_to :user
	has_and_belongs_to_many :game_board_categories

	scope :unvalidated_by_user, -> (user_id) { joins("LEFT JOIN answer_validations ON answer_validations.answer_id = answers.id AND answer_validations.user_id = #{user_id}").group('answers.id').having('COUNT(answer_validations.id) = 0') }
	scope :by_user, -> (user_id) { where(:user_id => user_id) }
	scope :with_upvotes, -> { includes("answer_validations").where( answer_validations: { is_valid: true }) }
	scope :with_downvotes, -> { includes("answer_validations").where( answer_validations: { is_valid: false }) }
	scope :with_confirmed_downvotes, -> { includes("answer_validations").where( "answer_validations.is_valid = 0 AND answer_validations.num_upvotes > 0 AND answer_validations.num_upvotes > answer_validations.num_downvotes" ) }
	scope :filter, -> (ids) { where("answer_validations.id NOT IN (?)", ids.join(",")) }
	scope :exclude, -> (ids) { where("answers.id NOT IN (#{ids.join(',')})") }
	scope :with_tag, -> (tag) { includes("tags").where("? IN (SELECT name FROM tags WHERE answers.id = tags.object_id AND tags.object_type = 'answer')", tag) }

	def update_tags(new_tags)
		self.tags.select{|t| !t.new_record?}.each do |t|
			t.destroy
		end
		new_tags.each do |t|
			tag = Tag.new({name: t})
			self.tags << tag
		end
	end

	def upvotes
		self.answer_validations.valid.length
	end

	def downvotes
		self.answer_validations.invalid.length
	end

	def tags_combined
		self.tags.map{|t| t.name }.join(", ")
	end
end
