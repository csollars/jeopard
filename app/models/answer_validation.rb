class AnswerValidation < ActiveRecord::Base
	belongs_to :user
	belongs_to :answer
	has_many :votes, as: :votable

	scope :valid, -> { where(:is_valid => true) }
	scope :invalid, -> { where(:is_valid => false) }
	scope :by_answer, -> (answer_id) { where(:answer_id => answer_id) }

	def display_reason
		case self.reason
		when 'incorrect_answer'
			return "Incorrect Answer"
		when 'malformed_question'
			return "Badly formed question"
		when 'unintelligable'
			return "Unreadable question"
		when 'opinion'
			return "Opinion"
		when 'inappropriate'
			return "Inappropriate"
		when 'bad_reference'
			return "Incorrect or inappropriate reference"
		when 'bad_tags'
			return "Miscategorized"
		end
	end
end
