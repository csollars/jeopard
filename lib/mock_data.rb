require 'json'

class MockData
	def self.run()
		f = File.open("/root/jeopardy/tmp/mock_users.json")
		users = JSON.load(f.read())
		users.each do |u|
			new_user = User.new(u)
			new_user.password_confirmation = new_user.password
			new_user.save!
		end

		users = User.all
		tag_names = ["Stuff", "Things", "Interesting", "facts", "history", "trivia"]

		bad_answer_reasons = ['incorrect_answer','malformed_question','unintelligable','opinion','inappropriate','bad_reference','bad_tags']

		f = File.open("/root/jeopardy/tmp/mock_answers.json")
		answers = JSON.load(f.read())
		answers.each do |ans|
			new_answer = Answer.new(ans)
			new_answer.question = new_answer.question.truncate(150)
			new_answer.user_id = Random.rand(users.length)
			new_answer.difficulty_votes = 10
			new_answer.save!

			(1..15).each do |i|
				user_id = Random.rand(users.length)
				next if user_id == new_answer.user_id
				is_valid = Random.rand(2) == 1 ? true : false
				validation = AnswerValidation.new({
					:answer_id => new_answer.id,
					:user_id => user_id,
					:is_valid => is_valid,
					:reference => "http://example.com",
					:num_upvotes => Random.rand(20),
					:reason => is_valid ? "" : bad_answer_reasons[Random.rand(bad_answer_reasons.length)],
					:correct_answer => is_valid ? "" : "Fourty Two"
					})
				validation.save!
			end

			tag = Tag.new({
				:name => tag_names[Random.rand(tag_names.length)],
				:object_type => "answer",
				:object_id => new_answer.id
				})
			tag.save!
		end
	end
end