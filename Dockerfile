FROM ctsollars/ruby2-mysql

ADD docker/add_jeopardy_db.sh /root/add_jeopardy_db.sh
RUN /bin/bash /root/add_jeopardy_db.sh
ADD docker/jeopardy_daemon.sh /usr/local/sbin/jeopardy_daemon.sh
RUN chmod +x /usr/local/sbin/jeopardy_daemon.sh
ADD docker/gems_and_migrations.sh /usr/local/sbin/gems_and_migrations.sh
RUN chmod +x /usr/local/sbin/gems_and_migrations.sh
RUN /usr/local/sbin/gems_and_migrations.sh
ADD docker/update.sh /usr/local/sbin/update.sh
RUN chmod +x /usr/local/sbin/update.sh

ADD docker/prompt.sh /usr/local/sbin/prompt.sh
RUN chmod +x /usr/local/sbin/prompt.sh

ADD docker/console.sh /usr/local/sbin/console.sh
RUN chmod +x /usr/local/sbin/console.sh

ADD docker/mysql_console.sh /usr/local/sbin/mysql_console.sh
RUN chmod +x /usr/local/sbin/mysql_console.sh

EXPOSE 3000
ENTRYPOINT ["/usr/local/sbin/jeopardy_daemon.sh"]
