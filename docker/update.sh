#!/bin/bash

/usr/sbin/mysqld &
sleep 2
cd /root/jeopardy
bundle install
rake db:migrate
