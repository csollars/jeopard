#!/bin/bash

git clone https://csollars@bitbucket.org/csollars/jeopard.git /root/jdev
cd /root/jdev
bundle install
/usr/sbin/mysqld &
rake db:migrate
cd /
rm -Rf /root/jdev
