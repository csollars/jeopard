#!/bin/bash

/usr/sbin/mysqld &
sleep 2
mysql -u root -e "CREATE DATABASE jeopardy; GRANT ALL PRIVILEGES on jeopardy.* TO 'jeopardy'@'localhost' IDENTIFIED BY 'jeopardy'"
killall mysqld
